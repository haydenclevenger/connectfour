public class Pair {
	private int x;
	private int y;

	public Pair() {
		this.x = 0;
		this.y = 0;
	}

	public Pair(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

	public void setX(int newX) {
		this.x = newX;
	}

	public void setY(int newY) {
		this.y = newY;
	}

	public boolean equals(Pair check)
	{
		boolean same = false;
		if(check.getX() == this.getX()
		&& check.getY() == this.getY())
			same = true;
		return true;
	}
}
