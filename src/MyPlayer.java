import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Hayden Clevenger
 * @email: haydenclevenger@sandiego.edu
 * 13th May, 2015
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

	if(gameBoard.getBoard()[0][3] == 0)
		return 3;
	//searching for move with no preset alpha and beta
        Move bestMove = search(gameBoard, 8, this.playerNumber, -2, 2);
	//keeps game going as long as possible
	if(bestMove.value == -1.0)
	     bestMove = search(gameBoard, 1, this.playerNumber, -2, 2);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber, double alpha, double beta) {

	long time = System.nanoTime();

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }


            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
		    //updating alpha/beta
   		    alpha = Math.max(thisMove.value, alpha);
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
		    //updating alpha/beta
		    alpha = Math.max(thisMove.value, alpha);
                } else {
                    // Loss
                    thisMove.value = -1.0;
		    //updating alpha/beta
		    alpha = Math.max(thisMove.value, alpha);
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);
		//updating alpha/beta
		alpha = Math.max(thisMove.value, alpha);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1), -beta, -alpha);
                thisMove.value = -responseMove.value;
		//updating alpha/beta
		alpha = Math.max(thisMove.value, alpha);
            }


            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);

	    //alpha beta pruning skip
	    if(alpha >= beta) {
		break;
	    }

	    //timing check
	    if((System.nanoTime()-time)/10 > 800000000) {
		System.out.println("System Timeout: " + (double)(System.nanoTime()-time)/1000000000 + " sec");
		break;
	    }
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
	int[][] board = gameBoard.getBoard();
	ArrayList<Pair> winningMoves = new ArrayList<Pair>();
	ArrayList<Pair> losingMoves = new ArrayList<Pair>();

	//looping through spaces checking them for 'winning-ness'
	for(int i = 0; i < 7; i++) {
		for(int j = 0; j < 7; j++) {
			//calculating winning moves
			if(horizontal(board, playerNumber, i, j)
			|| vertical(board, playerNumber, i, j)
			|| diagonalRight(board, playerNumber, i, j)
			|| diagonalLeft(board, playerNumber, i, j)) {
				Pair winPair = new Pair();
				winPair.setX(i);
				winPair.setY(j);
				winningMoves.add(winPair);
			}
			//adding losing moves
			int opponent = ((playerNumber == 1) ? 2 : 1);
			if(horizontal(board, opponent, i, j)
			|| vertical(board, opponent, i, j)
			|| diagonalRight(board, playerNumber, i, j)
			|| diagonalLeft(board, playerNumber, i, j)) {
				Pair losePair = new Pair();
				losePair.setX(i);
				losePair.setY(j);
				losingMoves.add(losePair);
			}
		}
	}

	//checking if two consecutive winning moves
	Pair checkPair = new Pair();
	double win = 0;
	if(winningMoves.size() > 1) {
		for(int i = 0; i < winningMoves.size(); i++) {
			//checking above
			checkPair.setX(winningMoves.get(i).getX()+1);
			checkPair.setY(winningMoves.get(i).getY());
			if(winningMoves.contains(checkPair)) {
				win = 1;


			}
			//checking below
			checkPair.setX(checkPair.getX()-2);
			if(winningMoves.contains(checkPair)) {
				win = 1;
			}
			//checking if invalid threat (opponent has win lower in column)
			if(win == 1) {
				for(int j = checkPair.getX(); j >= 0; j--) {
					if(losingMoves.contains(checkPair))
						win = 0;
					checkPair.setX(checkPair.getX()-1);
				}
			if(win == 1)
				return win;
			}
		}
	}
	win = 0;
	//checking if two consecutive losing moves
	if(losingMoves.size() > 1) {
		for(int i = 0; i < losingMoves.size(); i++) {
			//checking above
			checkPair.setX(losingMoves.get(i).getX()+1);
			checkPair.setY(losingMoves.get(i).getY());
			if(losingMoves.contains(checkPair))
				win = -1;
			//checking below
			checkPair.setX(checkPair.getX()-2);
			if(losingMoves.contains(checkPair))
				win = -1;
			//checking if invalid threat
			if(win == -1) {
				for(int j = checkPair.getX(); j >= 0; j--) {
					if(winningMoves.contains(checkPair))
						win = 0;
					checkPair.setX(checkPair.getX()-1);
				}
			if(win == -1)
				return win;
			}
		}
	}
	//neither player has trap so return fraction of win/lose spots

	//if both players have 0 potential wins/loses
	if(winningMoves.size() == 0 && losingMoves.size() == 0)
		return 0;

	//if player has wins but no losses
	else if(losingMoves.size() == 0) {
		win = winningMoves.size();
		if(win == 1)
			return 0.5;
		else
			return (1 - (1/win));
	}

	//if player has losses but no wins
	else if(winningMoves.size() == 0) {
		win = losingMoves.size();
		if(win == 1)
			return -0.5;
		else
			return - (1 - (1/win));
	}

	//some combination of wins/loses
	else {
		double odds = (double)winningMoves.size();
		win = (double)losingMoves.size();					
		odds = odds/win;
		if(odds > 1) {
			odds = (double)losingMoves.size();
			win = (double)winningMoves.size();
			odds = odds/win;
		}
		odds = 1 - odds;
		odds = (losingMoves.size() > winningMoves.size()) ? -odds : odds;
		return odds;
	}
   }

	/*Method to check if a spot is a winning space in a horizontal plane
	@gameBoard is the 2D array representing the connect four game board
	@playerNumber is the player [1,2] who we are calculating odds for
	@i,j is the coordinate of the spot on the board we are checking
	@returns true if the spot is a potential winner
	*/
	private boolean horizontal(int[][] gameBoard, int playerNumber, int i, int j) {
		int counter = 0;
		//cycling through row and increasing counter if connecting four possible
		for(int k = 0; k < 7; k++) {
			if(k == j || gameBoard[i][k] == playerNumber)
				counter++;
			else
				counter = 0;
			if(counter == 4)
				return true;
		}
		return false;
	}

	/*Method to check if a spot is a winning space in a vertical plane
	@gameBoard is the 2D array representing the connect four game board
	@playerNumber is the player [1,2] who we are calculating odds for
	@i,j is the coordinate of the spot on the board we are checking
	@returns true if the spot is a potential winner
	*/
	private boolean vertical(int[][] gameBoard, int playerNumer, int i, int j) {
		int counter = 0;
		//cycling through columns and increasing counter if connecting four possible
		for(int k = 0; k < 7; k++) {
			if(k == i || gameBoard[k][j] == playerNumber)
				counter++;
			else
				counter = 0;
			if(counter == 4)
				return true;
		}
		return false;
	}
				
	/*Method to check if a spot is a winning space in a diagonal plane
	@gameBoard is the 2D array representing the connect four game board
	@playerNumber is the player [1,2] who we are calculating odds for
	@i,j is the coordinate of the spot on the board we are checking
	@returns true if the spot is a potential winner
	*/
	private boolean diagonalRight(int[][] gameBoard, int playerNumer, int i, int j) {
		//check to see if spot is out of possible diagonal range
		if(Math.abs(i-j) > 3)
			return false;
		//setting up indices
		int counter = 0;
		int k, l;
		if(i-j > 0) {
			k = i-j;
			l = 0;
		}
		else {
			k = 0;
			l = j-i;
		}
		//cycling through diagonally up right to see if connecting four possible
		for(k = k, l = l; k < 7 && l < 7; k++, l++) {
			if((k == i && l == j) || gameBoard[k][l] == playerNumber)
				counter++;
			else
				counter = 0;
			if(counter == 4)
				return true;
		}
		return false;
	}

	/*Method to check if a spot is a winning space in a diagonal plane
	@gameBoard is the 2D array representing the connect four game board
	@playerNumber is the player [1,2] who we are calculating odds for
	@i,j is the coordinate of the spot on the board we are checking
	@returns true if the spot is a potential winner
	*/
	private boolean diagonalLeft(int[][] gameBoard, int playerNumer, int i, int j) {
		//check to see if spot is out of possible diagonal range
		if(Math.abs(j-i) > 3)
			return false;
		//setting up indices
		int counter = 0;
		int k,l;
		if(i-j > 0) {
			k = i-j;
			l = 6;
		}
		else {
			k = j-i;
			l = 6;
		}
		//cycling through diagonally down left to see if connecting four possible
		for(k=k, l=l; k > -1 && l > -1; k--, l--) {
			if((k == i && l == j) || gameBoard[k][l] == playerNumber) {
				counter++;
			}
			else
				counter = 0;
			if(counter == 4)
				return true;
		}
		return false;
	}
		

    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}
