# README #

### Description ###

This repository contains a simple Connect Four AI.
The game can be played between any combination of AI and human players
and operates completely on the command line inside the terminal window.

### Future improvements ###

In the future I would like to improve upon the intelligence of my AI.
Adding lookup tables for known game states would be a major improvement.

### How do I get set up? ###

To run simply clone the repository, navigate to the src folder and run Main.java
To configure human and computer players edit the Player p1 and p2 variables in Main.java

### Contribution guidelines ###

All contributions and suggestions welcome!

### Author ###

Hayden Clevenger
hayden.clev@gmail.com